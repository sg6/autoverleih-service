/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fhautoverleih;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author stefan
 */
@Entity
@Table(name = "bookings")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Bookings.findAll", query = "SELECT b FROM Bookings b"),
    @NamedQuery(name = "Bookings.findById", query = "SELECT b FROM Bookings b WHERE b.id = :id"),
    @NamedQuery(name = "Bookings.findByFromDate", query = "SELECT b FROM Bookings b WHERE b.fromDate = :fromDate"),
    @NamedQuery(name = "Bookings.findByToDate", query = "SELECT b FROM Bookings b WHERE b.toDate = :toDate"),
    @NamedQuery(name = "Bookings.findByCountKm", query = "SELECT b FROM Bookings b WHERE b.countKm = :countKm"),
    @NamedQuery(name = "Bookings.findByFkCarId", query = "SELECT b FROM Bookings b WHERE b.fkCarId = :fkCarId"),
    @NamedQuery(name = "Bookings.findByActive", query = "SELECT b FROM Bookings b WHERE b.active = :active")})
public class Bookings implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "firstname")
    private String firstname;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "lastname")
    private String lastname;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "address")
    private String address;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "fromDate")
    private String fromDate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "toDate")
    private String toDate;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "countKm")
    private Double countKm;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fkCarId")
    private int fkCarId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "active")
    private short active;

    public Bookings() {
    }

    public Bookings(Integer id) {
        this.id = id;
    }

    public Bookings(Integer id, String firstname, String lastname, String address, String fromDate, String toDate, int fkCarId, short active) {
        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
        this.address = address;
        this.fromDate = fromDate;
        this.toDate = toDate;
        this.fkCarId = fkCarId;
        this.active = active;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public Double getCountKm() {
        return countKm;
    }

    public void setCountKm(Double countKm) {
        this.countKm = countKm;
    }

    public int getFkCarId() {
        return fkCarId;
    }

    public void setFkCarId(int fkCarId) {
        this.fkCarId = fkCarId;
    }

    public short getActive() {
        return active;
    }

    public void setActive(short active) {
        this.active = active;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Bookings)) {
            return false;
        }
        Bookings other = (Bookings) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "fhautoverleih.Bookings[ id=" + id + " ]";
    }
    
}

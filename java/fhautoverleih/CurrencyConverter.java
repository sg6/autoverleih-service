package fhautoverleih;


import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;

/*
 * To change this license header, choose License Headers in Project Properties. 
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author amita priwasnig
 */
public class CurrencyConverter {    
    
      public static SOAPMessage callCurrencyConverter(SOAPMessage msg) throws SOAPException{
        SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
        SOAPConnection soapConnection = soapConnectionFactory.createConnection();

        String url = "http://sintratecalc.azurewebsites.net/service.asmx";
        
        SOAPMessage soapResponse = soapConnection.call(msg, url);
        soapConnection.close();
        return soapResponse;
    }

    public static SOAPMessage getAllCurrencies() throws Exception {
        MessageFactory messageFactory = MessageFactory.newInstance();
        SOAPMessage soapMessage = messageFactory.createMessage();
        SOAPPart soapPart = soapMessage.getSOAPPart();

        String serverURI = "http://sintratecalc.azurewebsites.net/";
        
        // SOAP Envelope
        SOAPEnvelope envelope = soapPart.getEnvelope();
        envelope.addNamespaceDeclaration("ns1", serverURI);
        
        // SOAP Body
        SOAPBody soapBody = envelope.getBody();
        MimeHeaders headers = soapMessage.getMimeHeaders();
        headers.addHeader("SOAPAction", serverURI  + "getAllCurrencies");        
        soapMessage.saveChanges();
        return soapMessage;
    }
    
    public static SOAPMessage getCurrencyRate(String cur1 , String cur2) throws Exception {
        MessageFactory messageFactory = MessageFactory.newInstance();
        SOAPMessage soapMessage = messageFactory.createMessage();
        SOAPPart soapPart = soapMessage.getSOAPPart();

        String serverURI = "http://sintratecalc.azurewebsites.net/";
        //String serverURI = "http://localhost:34466/";
        
        // SOAP Envelope
        SOAPEnvelope envelope = soapPart.getEnvelope();
        envelope.addNamespaceDeclaration("ns1", serverURI);
        
        // SOAP Body
        SOAPBody soapBody = envelope.getBody();
        SOAPElement soapBodyElem = soapBody.addChildElement("getCurrencyRate","ns1");
        SOAPElement soapBodyElem2 = soapBodyElem.addChildElement("cur1","ns1");
        soapBodyElem2.addTextNode(cur1);
        SOAPElement soapBodyElem3 = soapBodyElem.addChildElement("cur2","ns1");
        soapBodyElem3.addTextNode(cur2);
        
        
        MimeHeaders headers = soapMessage.getMimeHeaders();
        headers.addHeader("SOAPAction", serverURI  + "getCurrencyRate");

        soapMessage.saveChanges();
        return soapMessage;
    }
    
}

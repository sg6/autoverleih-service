/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fhautoverleih.service;

import fhautoverleih.Bookings;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import fhautoverleih.StringOutputStream;
import fhautoverleih.CurrencyConverter;
import java.io.ByteArrayOutputStream;

/**
 *
 * @author stefan
 */
@Stateless
@Path("fhautoverleih.bookings")
public class BookingsFacadeREST extends AbstractFacade<Bookings> {
    
    public static double PRICE_PER_DAY = 49.0;
    public static double INCLUDED_KM_PER_DAY = 100.0;
    public static double PRICE_PER_ADDITIONAL_KM = 0.19;
    
    @PersistenceContext(unitName = "FHAutoVerleihPU")
    private EntityManager em;

    public BookingsFacadeREST() {
        super(Bookings.class);
    }

    @POST
    @Override
    @Consumes({"application/xml", "application/json"})
    public void create(Bookings entity) {
        super.create(entity);
    }

    @PUT
    @Path("{id}")
    @Consumes({"application/xml", "application/json"})
    public void edit(@PathParam("id") Integer id, Bookings entity) {
        super.edit(entity);
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") Integer id) {
        super.remove(super.find(id));
    }

    @GET
    @Path("{id}")
    @Produces({"application/xml", "application/json"})
    public Bookings find(@PathParam("id") Integer id) {
        return super.find(id);
    }

    @GET
    @Override
    @Produces({"application/xml", "application/json"})
    public List<Bookings> findAll() {
        return super.findAll();
    }

    @GET
    @Path("{from}/{to}")
    @Produces({"application/xml", "application/json"})
    public List<Bookings> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces("text/plain")
    public String countREST() {
        return String.valueOf(super.count());
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    @GET
    @Path("currencies")
    @Produces({"text/plain"})
    public String getAllCurrencies() {
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            StringOutputStream s = new StringOutputStream();
            
            CurrencyConverter.callCurrencyConverter(CurrencyConverter.getAllCurrencies()).writeTo(s);

            System.out.println(CurrencyConverter.callCurrencyConverter(CurrencyConverter.getAllCurrencies()).getContentDescription());
            return s.toString();
        }
        catch(Exception e) {
            return null;
        }
    }
    
        
    @GET
    @Path("currency/{cur1}/{cur2}")
    @Produces({"text/plain"})
    public String getCurrencyRate(@PathParam("cur1") String cur1, @PathParam("cur2") String cur2) {
        try {
            StringOutputStream s = new StringOutputStream();
            CurrencyConverter.callCurrencyConverter(CurrencyConverter.getCurrencyRate(cur1, cur2)).writeTo(s);

            return s.toString();
        }
        catch(Exception e) {
            return null;
        }
    }
    
    @GET
    @Path("getPrice/{days}/{km}")
    @Produces({"text/plain"})
    public double getPrice(@PathParam("days") double days, @PathParam("km") double km) {
        double _price = PRICE_PER_DAY * days;
        double _availableKm = days * INCLUDED_KM_PER_DAY;
        if(km > _availableKm) {
            _price += (km - _availableKm) * PRICE_PER_ADDITIONAL_KM;
        }
        
        return _price;
    }

    
}
